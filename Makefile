CC=mpicc
CFLAGS=-O3 -Wall -g 

.c.o:
	$(CC) -c $(CFLAGS) $<

all: bin2ppm diffbin pingpong colcopy karman zip # run display # karman-par

run:
	mpirun -n 4 ./karman
	./bin2ppm -i karman.bin -o karman.ppm
	convert karman.ppm karman.png
	open karman.png

display:
	./bin2ppm -i karman.bin -o karman.ppm
	convert karman.ppm karman.png
	open karman.png

zip:
	zip 0935112-cs402-code.zip *.h *.c

clean:
	rm -f 0935112-cs402-code.zip
	rm -f bin2ppm diffbin pingpong colcopy karman karman-par *.o

karman: alloc.o boundary.o init.o karman.o simulation.o mylib.o
	$(CC) $(CFLAGS) -o $@ $^ -lm

karman-par: alloc.o boundary.o init.o karman-par.o simulation-par.o
	$(CC) $(CFLAGS) -o $@ $^ -lm

bin2ppm: bin2ppm.o alloc.o
	$(CC) $(CFLAGS) -o $@ $^ -lm

diffbin: diffbin.c
	$(CC) $(CFLAGS) -o $@ $^ -lm

pingpong: pingpong.o
	$(CC) $(CFLAGS) -o $@ $^

colcopy: colcopy.o alloc.o
	$(CC) $(CFLAGS) -o $@ $^

bin2ppm.o        : alloc.h datadef.h
boundary.o       : datadef.h
colcopy.o        : alloc.h
init.o           : datadef.h
karman.o         : alloc.h boundary.h datadef.h init.h simulation.h
karman-par.o     : alloc.h boundary.h datadef.h init.h simulation.h
simulation.o     : datadef.h init.h
simulation-par.o : datadef.h init.h
mylib.o			 : mylib.h 
