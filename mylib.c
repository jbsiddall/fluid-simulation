
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include "alloc.h"
#include "mylib.h"

Workspace *workspace;

void printMatrix(float ** matrix, int startCol, int startRow, int endCol, int endRow)
{
	int col, row;
	for(row = startRow; row <= endRow; row++) {
		for(col = startCol; col <= endCol; col++) {
			printf("%f, ", matrix[col][row]);
		}
		printf("\n");
	}
	fflush(stdout);
}

void initNetwork(int *argc, char ***argv, int imax, int jmax) {
	//printf("init network\n");

  MPI_Init(argc, argv);
  
  int rank;
  int nproc;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  workspace = (Workspace*) malloc(sizeof(Workspace));

  int cols = imax+2;
  int rows = jmax+2;

  int vimax = ceil((float)imax / nproc) * nproc;
  int col_alloc = vimax / nproc;
  int proc_lower_bound = rank * col_alloc + 1;
  int proc_upper_bound = proc_lower_bound + col_alloc -1;

  int type;

  if(nproc == 1) {
	  type = TYPE_ALONE;
  } else if(proc_lower_bound > imax) {
	  type = TYPE_VIRTUAL;
  } else if(proc_lower_bound == 1) {
	  type = TYPE_LEFT;
  } else if(proc_upper_bound >= imax & proc_lower_bound <= imax) {
	  type = TYPE_RIGHT;
  } else {
	  type = TYPE_MIDDLE;
  }

  if(proc_upper_bound > imax) {
	  proc_upper_bound = imax;
  }

  workspace->rank = rank;
  workspace->lowerBound = proc_lower_bound;
  workspace->upperBound = proc_upper_bound;
  workspace->type = type;
  workspace->cols = cols;
  workspace->rows = rows;
  workspace->imax = imax;
  workspace->jmax = jmax;

  printf("workspace %d: lb=%d, ub=%d, type=%d imax=%d, jmax=%d\n", 
		 rank, 
		 proc_lower_bound,
		 proc_upper_bound,
		 type,
	     imax,
	     jmax);
}

void sendFloatColumn(float** matrix, int col, int rows, int receiver)
{
	MPI_Send(&matrix[col][0], rows, MPI_FLOAT, receiver, 0, MPI_COMM_WORLD);
}

void receiveFloatColumn(float** matrix, int col, int rows, int receiver)
{
	MPI_Status status;
	MPI_Recv(&matrix[col][0], rows, MPI_FLOAT, receiver, 0, MPI_COMM_WORLD, &status);
}

void localSharingCycle(float** p) { 

	int rankL = workspace->rank-1;
	int rankR = workspace->rank+1;

	int lb = workspace->lowerBound;
	int ub = workspace->upperBound;

	int rows = workspace->rows;

	// share your columns: lower_boundary, upper_boundary
	// receieve columns: lower_boundary-1, upper_boundary+1
	// odd ranks: share(left, right), recv(right, left)
	// even ranks: recv(right, left), send(left, right)

	if(ODD(workspace->rank)) { // ODD
		if(HAS_LEFT) { sendFloatColumn(p, lb, rows, rankL); }
		if(HAS_RIGHT) { sendFloatColumn(p, ub, rows, rankR); } 
		if(HAS_RIGHT) { receiveFloatColumn(p, ub+1, rows, rankR); } 
		if(HAS_LEFT) { receiveFloatColumn(p, lb-1, rows, rankL); } 
	} else { // EVEN
		if(HAS_RIGHT) { receiveFloatColumn(p, ub+1, rows, rankR); } 
		if(HAS_LEFT) { receiveFloatColumn(p, lb-1, rows, rankL); } 
		if(HAS_LEFT) { sendFloatColumn(p, lb, rows, rankL); }
		if(HAS_RIGHT) { sendFloatColumn(p, ub, rows, rankR); }
	}
	
}

void localSharing(float **p)
{
	localSharingCycle(p);
}

#define LOG(msg) { if(workspace->rank != -1) { printf(msg); fflush(stdout); } }

void globalSharing(float **p)
{

	if(workspace->rank != 0) {
		MPI_Send(&workspace->lowerBound, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
		MPI_Send(&workspace->upperBound, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
		
		if(workspace->type != TYPE_VIRTUAL && workspace->type != TYPE_ALONE) {
			int col;
			for(col = workspace->lowerBound ; col <= workspace->upperBound; col++) {
				sendFloatColumn(p, col, workspace->rows, 0);
			}
		}
	} else { // is root
		int nprocs;
		MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
		
		int rank;
		for(rank = 1; rank < nprocs; rank++) {
			int col;
			int upperCol;
			MPI_Status status;
			MPI_Recv(&col, 1, MPI_INT, rank, 0, MPI_COMM_WORLD, &status);
			MPI_Recv(&upperCol, 1, MPI_INT, rank, 0, MPI_COMM_WORLD, &status);
			for(; col <= upperCol; col++) {
				if(col >= 0 && col < workspace->cols) {
					receiveFloatColumn(p, col, workspace->rows, rank);
				}
			}
		}
	}

	MPI_Bcast(&p[0][0], workspace->cols*workspace->rows, MPI_FLOAT, 0, MPI_COMM_WORLD);

}

void shareRes(double* res)
{
	
	MPI_Allreduce(MPI_IN_PLACE, res, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

}

void finaliseNetwork()
{
  MPI_Finalize();
}
