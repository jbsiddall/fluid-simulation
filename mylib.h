

#ifndef MYLIB_H
#define MYLIB_H

#define TYPE_ALONE 1
#define TYPE_LEFT 2
#define TYPE_MIDDLE 4
#define TYPE_RIGHT 8
#define TYPE_VIRTUAL 16

#define EVEN(n) ((n)%2)
#define ODD(n) ((n)%2 == 1)

#define HAS_LEFT ((workspace->type == TYPE_MIDDLE) || (workspace->type == TYPE_RIGHT))
#define HAS_RIGHT ((workspace->type == TYPE_MIDDLE) || (workspace->type == TYPE_LEFT))

typedef struct
{
	int rank;
	int lowerBound;
	int upperBound;
	int type; // LEFT | MIDDLE | RIGHT | VIRTUAL
	int cols;
	int rows;
	int imax;
	int jmax;
} Workspace;


// sends adjacent cols to correct neighbour, then recieves adjacent cols, odd ranks send first
void localSharing(float **p);
void globalSharing(float **p);
void initNetwork(int *argc, char ***argv, int imax, int jmax);
void finaliseNetwork();
void shareRes(double* localRes);
#endif
